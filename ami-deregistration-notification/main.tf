



resource "null_resource" "lambda_package" {
	triggers = {
		always_run = "${timestamp()}"
	}
	
	provisioner "local-exec" {
		command = <<EOT
			cd ${path.module}/src/
			chmod +x package.sh
			sh package.sh 
		EOT
	}
}

data "archive_file" "deregistration_lambda_package" {
	depends_on = ["null_resource.lambda_package"]
	type = "zip"
	source_dir = "${path.module}/src/"
	output_path = "${path.module}/src/ami_deregistration.zip"
	excludes = ["mailer-0.8.1.zip", "ami_deregistration.zip", "package.sh"]
}

resource "aws_lambda_function" "lambda" {
	filename      = "${data.archive_file.deregistration_lambda_package.output_path}"
	function_name = "lambda_function_name"
	role          = "arn:aws:iam::302228741901:role/service-role/bundle-list-role-nqr63xx9"
	handler       = "a.handler"
	source_code_hash = "${data.archive_file.deregistration_lambda_package.output_base64sha256}"
	runtime = "python3.8"
}

output "path" {
	value = "${data.archive_file.deregistration_lambda_package.output_path}"
}